$(function () {
blogCekveGoster();
})

function blogCekveGoster() {
    $(".blogTablo").html("");
    $.ajax({
        url:"../restapi/blog.php",
        type:"GET",
        dataType:"json",
        data:{"tumBloglar":123},
        success:function (veri) {
            for (var i=0;i<veri.bloglar.length;i++){
                $(".blogTablo").append(" <tr>\n" +
                    "                            <td>"+veri.bloglar[i].blogId+"</td>\n" +
                    "                            <td>"+veri.bloglar[i].blogBaslik+"</td>\n" +
                    "                            <td><a onclick=\"guncelleModalDoldur("+veri.bloglar[i].blogId+")\" class='btn btn-outline-success'>Güncelle</a></td>\n" +
                    "                        </tr>")
            }
        },
        error:function (hata) {
            $(".blogTablo").html(" <tr class='text-center'>\n" +
                "                            <td colspan='2'>Blog Yazısı Bulunmuyor</td>\n" +

                "                        </tr>")
        }
    })
}

function guncelleModalDoldur(gelenId) {
var id = gelenId;

$.ajax({
    url:"../restapi/blog.php",
    type:"GET",
    dataType: "json",
    data:{"gelenBlogId":id},
    success:function (veri) {
        var baslik = veri.bloglar.blogBaslik;
        var metin = veri.bloglar.blogMetin;
        $("#eskiBlogId").val(id);
        CKEDITOR.instances.eskiBlogMetin.setData(metin);
        $("#eskiBlogBaslik").val(baslik);
    },
    error:function (hata) {
        alertify.error(hata.responseJSON.hataMesaji);
        console.log(hata.responseJSON.hataMesaji);
    },
    complete:function () {
        $("#blogGuncelleModal").modal("show");
    }
})

}

function blogGuncelle() {
    var metin = CKEDITOR.instances.eskiBlogMetin.getData();
    var id = $("#eskiBlogId").val();
    var baslik = $("#eskiBlogBaslik").val();

    $.ajax({
        url:"../restapi/blog.php",
        type:"PUT",
        dataType:"json",
        contentType:"application/json",
        data:JSON.stringify({
            "id":id,
            "baslik":baslik,
            "metin":metin
        }),
        success:function (veri) {
            alertify.success(veri.bloglar);
        },
        error:function (hata) {
            alertify.error(hata.responseJSON.hataMesaji);
        },
        complete:function () {
            $("#blogGuncelleModal").modal("hide");
            blogCekveGoster();
        }
    })
}

function blogEkle() {
    var baslik = $("#blogBaslik").val();
    var metin = CKEDITOR.instances.blogMetin.getData();

    $.ajax({
        url:"../restapi/blog.php",
        type:"POST",
        dataType:"json",
        data:{
            "baslik":baslik,
            "metin":metin
        },
        success:function (veri) {
            alertify.success(veri.bloglar);
        },
        error:function (hata) {
            alertify.error(hata.responseJSON.hataMesaji);
        },
        complete:function () {
            $("#blogEkleModal").modal("hide");
            blogCekveGoster();
        }
    })
}