$(function () {

bloglariCek();
})

function blogBul(){
    var arananKelime = $("#aramaKismi").val();
    $(".blogKismi").html("");
    $(".blogKismi").show();
    $(".blogDetay").hide();
    $("#aramaKismi").show();
    $.ajax({
        url:"../restapi/blog.php",
        type:"GET",
        dataType:"json",
        data:{
            arananBlog:arananKelime
        },
        success:function(veri){
            for (var i=0;i<veri.bloglar.length;i++){
                $(".blogKismi").append(" <div class=\"col-md-12 mt-3\">\n" +
                    "                <div class=\"card\">\n" +
                    "                    <div class=\"card-header\">\n" +
                    "                        Blog Yazısı\n" +
                    "                    </div>\n" +
                    "                    <div class=\"card-body\">\n" +
                    "                        <h5 class=\"card-title\">"+veri.bloglar[i].blogBaslik+"</h5>\n" +
                    "                        <a onclick=\"blogDetay("+veri.bloglar[i].blogId+")\" class=\"btn btn-primary\">Bloga Git</a>\n" +
                    "                    </div>\n" +
                    "                </div>\n" +
                    "            </div>")
                }
                alertify.success("Arama sonuçları listelendi.",2);
            },
            error:function(hata){
                $(".blogKismi").html(`<div class='col-md-6'>
                <div class='card bg-danger'>
                <div class='card-body text-white'>
                ${hata.responseJSON.hataMesaji}
                </div>
                </div>
                </div>`);
        }
    })
}

function bloglariCek() {
    $(".blogKismi").html("");
    $(".blogKismi").show();
    $(".blogDetay").hide();
    $("#aramaKismi").show();
    $.ajax({
        url:"../restapi/blog.php",
        type:"GET",
        dataType:"json",
        data:{"tumBloglar":123},
        success:function (veri) {
            for (var i=0;i<veri.bloglar.length;i++){
                $(".blogKismi").append(" <div class=\"col-md-12 mt-3\">\n" +
                    "                <div class=\"card\">\n" +
                    "                    <div class=\"card-header\">\n" +
                    "                        Blog Yazısı\n" +
                    "                    </div>\n" +
                    "                    <div class=\"card-body\">\n" +
                    "                        <h5 class=\"card-title\">"+veri.bloglar[i].blogBaslik+"</h5>\n" +
                    "                        <a onclick=\"blogDetay("+veri.bloglar[i].blogId+")\" class=\"btn btn-primary\">Bloga Git</a>\n" +
                    "                    </div>\n" +
                    "                </div>\n" +
                    "            </div>")
            }
        },
        error:function (hata) {

        }
    })
}

function blogDetay(gelenId) {
    yorumlariGetir();
    $("#aramaKismi").hide();
    var id = gelenId;
    $("#gizliId").val(id);
    $.ajax({
        url:"../restapi/blog.php",
        type:"GET",
        dataType:"json",
        data:{"gelenBlogId":id},
        success:function (veri) {
            $(".blogDetayBaslik").text("");
            $(".blogDetayMetin").html("");

            $(".blogDetayBaslik").text(veri.bloglar.blogBaslik);
            $(".blogDetayMetin").append(veri.bloglar.blogMetin);
        },
        error:function (hata) {
            $(".blogDetayBaslik").text("");
            $(".blogDetayMetin").html("");
            $(".blogDetayBaslik").text(hata.responseJSON.hataMesaji);
        },
        complete:function () {
            $(".blogDetay").show();
            $(".blogKismi").hide();
        }
    })
}

function yorumEkle() {
    var id = $("#gizliId").val();
    var ad = $("#adiniz").val();
    var metin = $("#yorum").val();

    $.ajax({
        url:"../restapi/yorum.php",
        type:"POST",
        dataType:"json",
        data:{"ad":ad,"yorum":metin,"blogId":id,durum:0},
        success:function (veri) {
            alertify.success(veri.yorumlar);
        },
        error:function (hata) {
            alertify.error(hata.responseJSON.hataMesaji);
        },
        complete:function () {
           $("#gizliId").val("");
           $("#adiniz").val("");
           $("#yorum").val("");
        }
    })
}

function yorumlariGetir() {
    var id = $("#gizliId").val();
    $(".yorumlarKismi").html("");
    $.ajax({
        url:"../restapi/yorum.php",
        type:"GET",
        dataType:"json",
        data:{
            "kullanici":123213,
            "id":id
        },
        success:function (veri) {
            for (var i=0;i<veri.yorumlar.length;i++){
                $(".yorumlarKismi").append(" <div class=\"yorumYazan border border-success\">\n" +
                    "                    <h3>"+veri.yorumlar[i].yorumYazan+"</h3>\n" +
                    "                    <p>"+veri.yorumlar[i].metin+"</p>\n" +
                    "                </div>")
            }
        },
        error:function (hata) {
            $(".yorumlarKismi").html("");
            $(".yorumlarKismi").append(" <div class=\"yorumYazan border border-success\">\n" +
                "                    <h3>"+hata.responseJSON.hataMesaji+"</h3>\n" +

                "                </div>")
        }
    })
}