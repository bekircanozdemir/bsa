$(function () {
adminleriGoster();
console.log("Sayfa açıldı");
});



function adminleriGoster(){
    $(".admintablo").html("");
    $("#adminlerim").show();
    $("#yorumlarim").hide();
    $("#yazarlarim").hide();
    $.ajax({
        url:"../restapi/admin.php",
        type:"GET",
        dataType:"json",
        data:{
            "tumadminler":223
        },
        success:function (veri) {
            console.log(veri);
            var yazi = "hasanmertermis buradadır";
            for(var i=0;i<veri.adminler.length;i++){
              /*  $(".admintablo").append(" <tr>\n" +
                    "                    <td>"+veri.adminler[i].adminId+"</td>\n" +
                    "                    <td>"+veri.adminler[i].adminMail+"</td>\n" +
                    "                    <td>"+veri.adminler[i].adminPass+"</td>\n" +
                    "<td><a class='btn btn-outline-danger  btnsil' >Sil</a></td>"+
                        "                </tr>");*/

                $(".admintablo").append(" <tr>\n" +
                    "                    <td>"+veri.adminler[i].adminId+"</td>\n" +
                    "                    <td>"+veri.adminler[i].adminMail+"</td>\n" +
                    "                    <td>"+veri.adminler[i].adminPass+"</td>\n" +
                    "                    <td><a class='btn btn-outline-danger' onclick=\"adminSil("+veri.adminler[i].adminId+")\">Sil</a></td>\n" +
                    "                </tr>")

            }
        },
        error:function (hata) {
            console.log(hata);
        }


    })


}

function adminSil(gelenId) {
    var id = gelenId;

    $.ajax({
        url:"../restapi/admin.php?gelenId="+id,
        type: "DELETE",
        dataType: "json",
        success:function (veri) {
            alertify.success(veri.adminler);
            adminleriGoster();
        },
        error:function (hata) {
            alertify.error(hata);
            console.log(hata);
        }
    })

    console.log(gelenId);
}

function adminEkle() {


    var gelenMail = $("#gelenMail").val();
    var gelenPass = $("#gelenPass").val();

    $.ajax({
        url:"../restapi/admin.php",
        type:"POST",
        dataType:"json",
        data:{
            "gelenMail":gelenMail,
            "gelenPass":gelenPass
        },
        success:function (veri) {
            alertify.success(veri.adminler);
            $("#adminEkleModal").modal("hide");

        },
        error:function (hata) {
            alertify.error(hata.responseJSON.hataMesaji);
        },
        complete:function () {
            adminleriGoster();
        }
    })
}
function yazarlariGetir() {
    $("#adminlerim").hide();
    $("#yorumlarim").hide();

    $.ajax({
        url:"../restapi/yazar.php",
        type:"GET",
        dataType:"json",
        data:{"tumYazarlar":233},
        success:function (veri) {
            $(".yazartablo").html("");
            for(var i=0;i<veri.yazarlar.length;i++){
            var id = veri.yazarlar[i].yazarId;
            var mail = veri.yazarlar[i].yazarMail;
            var pass = veri.yazarlar[i].yazarPass;

            $(".yazartablo").append(" <tr>\n" +
                "                   \n" +
                "                        <td>"+veri.yazarlar[i].yazarId+"</td>\n" +
                "                        <td>"+veri.yazarlar[i].yazarMail+"</td>\n" +
                "                        <td>"+veri.yazarlar[i].yazarPass+"</td>\n" +
                "                        <td><a class='btn btn-outline-danger mr-3' onclick=\"yazarSil("+veri.yazarlar[i].yazarId+")\"><i class=\"fas fa-trash\"></i>&nbsp;Sil</a><a class='btn btn-outline-success' onclick=\"tekYazarGetir("+veri.yazarlar[i].yazarId+")\"><i class=\"fas fa-sync-alt\"></i>&nbsp;Güncelle</a></td>\n" +
                "                    </tr>")
            }

        },
        error:function (hata) {
            console.log(hata);
            $(".yazartablo").html("");
            $(".yazartablo").append(" <tr>\n" +
                "                        <td colspan=\"4\">Sistemde yazar bulunmuyor</td>\n" +
                "                    </tr>");
        },
        complete:function () {
            $("#yazarlarim").show();
        }
    })
}

function yazarSil(gelenId) {
    console.log(gelenId);
    var id = gelenId;

    $.ajax({
        url:"../restapi/yazar.php?gelenId="+id,
        type:"DELETE",
        dataType:"json",
        success:function (veri) {
            alertify.success(veri.yazarlar);
        },
        error:function (hata) {
            alertify.error(hata.responseJSON.hataMesaji);
        },
        complete:function () {
            yazarlariGetir();
        }
    })
}
function tekYazarGetir(gelenId) {
var id = gelenId;

$.ajax({
    url:"../restapi/yazar.php?tekliYazar="+id,
    type:"GET",
    dataType:"json",
    success:function (veri) {
        console.log(veri);
        var mail = veri.yazarlar.yazarMail;
        var pass = veri.yazarlar.yazarPass;
        $("#eskiYazarId").text(id);
        $("#yazarEskiMail").val(mail);
        $("#yazarEskiPass").val(pass);
    },
    error:function (hata) {
        console.log(hata.responseJSON.hataMesaji);
    },
    complete:function () {
        $("#yazarGuncelleModal").modal("show");
    }
})


}

function yazarGuncelle() {
   var id = $("#eskiYazarId").text();
   var mail = $("#yazarEskiMail").val();
   var pass = $("#yazarEskiPass").val();

   $.ajax({
       url:"../restapi/yazar.php",
       type:"PUT",
       dataType:"json",
       data:JSON.stringify({
           "yazarId":id,
           "yazarMail":mail,
           "yazarPass":pass
       }),
       success:function (veri) {
           alertify.success(veri.yazarlar);
       },
       error:function (hata) {
            alertify.error(hata.responseJSON.hataMesaji);
       },
       complete:function () {
            $("#yazarGuncelleModal").modal("hide");
            yazarlariGetir();
       }
   })
}

function yazarEkle() {
    var gelenMail = $("#yazarMail").val();
    var gelenPass = $("#yazarPass").val();

    $.ajax({
        url:"../restapi/yazar.php",
        type:"POST",
        dataType:"json",
        data:{
            "gelenMail":gelenMail,
            "gelenPass":gelenPass
        },
        success:function (veri) {
            alertify.success(veri.yazarlar);
            $("#yazarEkleModal").modal("hide");
        },
        error:function (hata) {
            alertify.error(hata.responseJSON.hataMesaji);
            console.log(hata.responseJSON.hataMesaji);

        },
        complete:function () {
            yazarlariGetir();

        }
    })
}

function yorumlariGetir() {
    $("#yorumlarim").show();
    $("#yazarlarim").hide();
    $("#adminlerim").hide();
    $(".yorumTablo").html("");
    $.ajax({
        url:"../restapi/yorum.php",
        type:"GET",
        dataType:"json",
        data:{
            "admin":123123
        },
        success:function (veri) {
            for (var i=0;i<veri.yorumlar.length;i++){
                $(".yorumTablo").append("  <tr>\n" +
                    "                        <td>"+veri.yorumlar[i].yorumId+"</td>\n" +
                    "                        <td>"+veri.yorumlar[i].metin+"</td>\n" +
                    "                        <td><a href=\"#\" class=\"btn btn-success\" onclick=\"yorumOnayla("+veri.yorumlar[i].yorumId+")\">Onayla</a></td>\n" +
                    "                    </tr>")
            }
        },
        error:function (hata) {

        }
    })
}

function yorumOnayla(gelenId) {
var id = gelenId;
$.ajax({
    url:"../restapi/yorum.php",
    type:"PUT",
    dataType:"json",
    data:JSON.stringify({
        "kabul":1,
        "id":id
    }),
    success:function (veri) {
        alertify.success(veri.yorumlar);
    },
    error:function (hata) {
        alertify.error(hata.responseJSON.hataMesaji);
    },
    complete:function () {
        yorumlariGetir();
    }
})
}


