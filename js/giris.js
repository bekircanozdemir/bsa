$(function () {
    alertify.success("Sayfa açıldı");
})


$("#adminGirisBtn").on("click",function () {
    var gelenMail = $("#adminMail").val();
    var gelenPass = $("#adminPass").val();

    $.ajax({
        url:"../restapi/admin.php",
        type:"GET",
        dataType:"json",
        data:{"adminMail":gelenMail,"adminPass":gelenPass},
        success:function (veri) {
            console.log(veri.adminler);
            $(window).attr('location', 'admin.php');
        },
        error:function (hata) {
            alertify.error(hata.responseJSON.hataMesaji,20);
        }
    })
});
$("#yazarGirisBtn").on("click",function () {
    var gelenMail = $("#yazarMail").val();
    var gelenPass = $("#yazarPass").val();


    $.ajax({
        url:"../restapi/yazar.php",
        type:"GET",
        dataType: "json",
        data:{"yazarMail":gelenMail,"yazarPass":gelenPass},
        success:function (veri) {
            $(window).attr('location', 'yazar.php')
        },
        error:function (hata) {
            alertify.error(hata.responseJSON.hataMesaji);
        }
    })

})