
    <html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="../vendor/twbs/bootstrap/dist/css/bootstrap.css">
        <title>Anasayfa</title>
        <link rel="stylesheet" href="../css/alertify.css">
        <link rel="stylesheet" href="../vendor/fortawesome/font-awesome/css/all.css">
        <link rel="stylesheet" href="../css/stil.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    </head>
    <body>
    <nav class="navbar sticky-top navbar-expand-lg menum">
        <a class="navbar-brand" href="#">Yazılım Blogum</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="#" onclick="bloglariCek()">Anasayfa <span class="sr-only">(current)</span></a>
                </li>


            </ul>
           
            <form class="form-inline my-2 my-lg-0">
            <div class="input-group mr-5">
            <input type="text" id="aramaKismi" class="form-control" placeholder="Blog Ara" aria-label="Recipient's username" aria-describedby="basic-addon2">
                <div class="input-group-append">
                    <button onclick="blogBul()" class="btn btn-outline-secondary" type="button"><i class="fas fa-search"></i></button>
                 </div>
            </div>
                <a href="giris.php" class="btn btn-outline-success">Giriş Sayfası</a>
            </form>
        </div>
    </nav>

    <div class="container">
        <div class="row mt-4 mb-3 blogKismi justify-content-center">

        </div>
        <div class="row mt-4 blogDetay justify-content-center mb-4">
            <div class="col-8 ">
               <h2 class="blogDetayBaslik text-center"></h2>
                <hr>
                <div class="blogDetayMetin border mt-3 mb-3 p-3"></div>
                <hr>
                <h3 class="text-center">Yorumlar</h3>
                <input type="text" hidden id="gizliId">
                    <div class="form-group">
                        <label for="adiniz">Adınız</label>
                        <input type="text" id="adiniz" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="yorum">Yorumunuz</label>
                        <textarea  id="yorum" cols="30" class="form-control" rows="10"></textarea>
                    </div>
                    <button class="btn btn-primary btn-block mb-3" onclick="yorumEkle()">Yorumu Gönder</button>

               <div class="yorumlarKismi">

               </div>

            </div>


        </div>
    </div>


    <div class="container-fluid footeralani">
        <div class="row bg-danger">

            <div class="col-md-4 listeler text-center">
                <h2>Sayfalar</h2>
                <ul class="list-group list-group-flush ">
                    <li class="list-group-item">Anasayfa</li>
                    <li class="list-group-item">Giriş Sayfası</li>


                </ul>
            </div>
            <div class="col-md-4 text-center"><img src="../img/logo.png" style="width: 10rem;" class="img-fluid" alt="">
            </div>
            <div class="col-md-4 listeler text-center">
                <h2>Projede Görev Alanlar</h2>
                <ul class="list-group list-group-flush ">
                    <li class="list-group-item">Bekir Can Uysal</li>
                    <li class="list-group-item">Bekir Can Uysal</li>
                    <li class="list-group-item">Bekir Can Uysal</li>

                </ul>
            </div>
        </div>
    </div>

    <script src="../js/jquery.js">
    </script>
    <script src="../js/kullanici.js"></script>
    <script src="../vendor/twbs/bootstrap/dist/js/bootstrap.js"></script>
    <script src="../js/alertify.js"></script>
    </body>
    </html>
