<?php
session_start();
if(isset($_SESSION["yazarmail"])) {

    ?>
    <!doctype html>
    <html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="../css/alertify.css">
        <link rel="stylesheet" href="../vendor/twbs/bootstrap/dist/css/bootstrap.css">
        <title>Hello, world!</title>
    </head>
    <body>

    <div class="modal fade" id="blogEkleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Blog Yazısı Ekle</h5>

                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Kapat</button>
                    <button type="button" class="btn btn-primary" onclick="blogEkle()">Ekle</button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="blogBaslik">Blog Başlığı</label>
                        <input type="text" class="form-control" id="blogBaslik">
                    </div>
                    <div class="form-control">

                        <textarea  id="blogMetin" class="ckeditor" cols="30" rows="10"></textarea>

                    </div>
                </div>
                <div class="modal-footer">

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="blogGuncelleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Blog Yazısını Güncelle</h5>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Kapat</button>
                    <button type="button" class="btn btn-primary" onclick="blogGuncelle()">Güncelle</button>
                </div>
                <div class="modal-body">
                    <input type="text" hidden id="eskiBlogId">
                    <div class="form-group">
                        <label for="eskiBlogBaslik">Blog Başlığı</label>
                        <input type="text" class="form-control" id="eskiBlogBaslik">
                    </div>
                    <div class="form-control">

                        <textarea  id="eskiBlogMetin" class="ckeditor" cols="30" rows="10"></textarea>

                    </div>
                </div>
                <div class="modal-footer">

                </div>
            </div>
        </div>
    </div>


    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="#">Yazar Yönetim Paneli</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="#">Bloglar <span class="sr-only">(current)</span></a>
                </li>



            </ul>
            <form class="form-inline my-2 my-lg-0">
                <button type="button" class="btn btn-outline-success" data-toggle="modal" data-target="#blogEkleModal">Blog Yazısı Ekle</button>
                <a href="session_destroy.php" class="btn btn-outline-danger">Çıkış Yap</a>
            </form>
        </div>
    </nav>


    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
            <div class="bloglar">
                <table class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <td>Blog Id</td>
                        <td>Blog Başlığı</td>
                        <td>İşlem</td>
                    </tr>
                    </thead>
                    <tbody class="blogTablo">

                    </tbody>
                </table>
            </div>
            </div>
        </div>
    </div>


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->

    <script src="../js/jquery.js"></script>
    <script src="../js/alertify.js"></script>
    <script src="../vendor/twbs/bootstrap/dist/js/bootstrap.js"></script>
    <script src="../ckeditor/ckeditor.js"></script>
    <script src="../js/yazar.js"></script>
     </body>
    </html>

    <?php
}else{
    header("location:giris.php");
}

    ?>
