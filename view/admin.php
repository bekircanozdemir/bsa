<?php
session_start();
if(isset($_SESSION["adminmail"])) {
    ?>


    <!doctype html>
    <html lang="en">
    <head>
        <link rel="stylesheet" href="../vendor/twbs/bootstrap/dist/css/bootstrap.css">

        <!-- Required meta tags -->
        <meta charset="utf-8">
        <link rel="stylesheet" href="../vendor/fortawesome/font-awesome/css/all.css">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="../css/alertify.css">
        <!-- Bootstrap CSS -->

        <title>Admin Sayfası</title>
    </head>
    <body>

    <div class="modal fade" id="yazarGuncelleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Yazar Güncelle</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p id="eskiYazarId"></p>
                    <div class="form-group">
                        <label for="yazarEskiMail">Yazar Mail</label>
                        <input type="text" class="form-control" id="yazarEskiMail">
                    </div>
                    <div class="form-group">
                        <label for="yazarEskiPass">Yazar Mail</label>
                        <input type="text" class="form-control" id="yazarEskiPass">
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Kapat</button>
                    <button type="button" class="btn btn-primary" onclick="yazarGuncelle()">Yazarı Güncelle</button>
                </div>
            </div>
        </div>
    </div>



    <div class="modal fade" id="yazarEkleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Yazar Ekle</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                   <div class="form-group">
                       <label for="yazarMail">Yazar Mail</label>
                       <input type="text" class="form-control" id="yazarMail">
                   </div>
                    <div class="form-group">
                        <label for="yazarPass">Yazar Mail</label>
                        <input type="text" class="form-control" id="yazarPass">
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Kapat</button>
                    <button type="button" class="btn btn-primary" onclick="yazarEkle()">Yazarı Ekle</button>
                </div>
            </div>
        </div>
    </div>



    <div class="modal fade" id="adminEkleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Yeni Admin Ekle</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                   <div class="form-group">
                       <label for="gelenMail">Admin Mail</label>
                       <input type="text" id="gelenMail" class="form-control">
                   </div>
                    <div class="form-group">
                        <label for="gelenPass">Admin Şifre</label>
                        <input type="password" id="gelenPass" class="form-control">
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Kapat</button>
                    <button type="button" class="btn btn-primary" onclick="adminEkle()">Admini Ekle</button>
                </div>
            </div>
        </div>
    </div>



    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand text-info" href="#">Admin Yönetim Paneli</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" onclick="adminleriGoster()" id="adminMenu" href="#">Adminler <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" onclick="yazarlariGetir()" id="yazarMenu" href="#">Yazarlar</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" onclick="yorumlariGetir()" href="#">Yorumlar</a>
                </li>


            </ul>
            <form class="form-inline my-2 my-lg-0">
                <button type="button" class="btn btn-outline-success mr-3" data-toggle="modal" data-target="#adminEkleModal">
                    Admin Ekle
                </button>
                <button type="button" class="btn btn-outline-primary mr-3" data-toggle="modal" data-target="#yazarEkleModal">
                    Yazar Ekle
                </button>
                <a href="session_destroy.php" class="btn btn-danger">Çıkış Yap</a>
            </form>
        </div>
    </nav>

    <div class="container">
        <div id="adminlerim" class="mt-3">
            <table class="table table-striped table-bordered">
                <thead>
                <tr>
                <td>Admin ID</td>
                <td>Admin Mail</td>
                <td>Admin Pass</td>
                <td>İşlem</td>
                </tr>
                </thead>
                <tbody class="admintablo">

                </tbody>
            </table>
        </div>
        <div id="yazarlarim" class="mt-3">
            <table class="table table-striped table-bordered">
                <thead>
                <tr>
                    <td>Yazar ID</td>
                    <td>Yazar Mail</td>
                    <td>Yazar Pass</td>
                    <td>İşlem</td>

                </tr>
                </thead>
                <tbody class="yazartablo">

                </tbody>
            </table>
        </div>
        <div id="yorumlarim">
            <table class="table table-striped table-bordered">
                <thead>
                <tr>
                    <td>Yorum ID</td>
                    <td>Yorum İçeriği</td>
                    <td>İşlem</td>


                </tr>
                </thead>
                <tbody class="yorumTablo">

                </tbody>
        </div>
    </div>



    <script src="../js/jquery.js">
    </script>
    <script src="../vendor/twbs/bootstrap/dist/js/bootstrap.js"></script>
    <script src="../js/alertify.js"></script>
    <script src="../js/admin.js"></script>
   </body>
    </html>

    <?php
}else{
    header("location:giris.php");
}
    ?>