

<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="../vendor/twbs/bootstrap/dist/css/bootstrap.css">
    <link rel="stylesheet" href="../vendor/fortawesome/font-awesome/css/all.css">
    <link rel="stylesheet" href="../css/alertify.css">
    <!-- Bootstrap CSS -->

    <title>Giriş Sayfası</title>
</head>
<body>
<div class="container">
    <h2 class="text-center mt-5"><u>Giriş Sayfası</u></h2>
    <div class="row justify-content-center mt-5">
        <div class="col-md-6 ">
            <div class="form-group">
                <label for="adminMail">Admin Mail</label>
                <input type="text" id="adminMail" class="form-control">
            </div>
            <div class="form-group">
                <label for="adminPass">Admin Şifresi</label>
                <input type="password" id="adminPass" class="form-control">
            </div>
            <button id="adminGirisBtn" class="btn btn-outline-primary btn-block"><i class="fas fa-lock"></i>&nbsp;Admin Girişi Yap</button>
        </div>
        <div class="col-md-6 border-left border-danger">
            <div class="form-group">
                <label for="yazarMail">Yazar Mail</label>
                <input type="text" id="yazarMail" class="form-control">
            </div>
            <div class="form-group">
                <label for="yazarPass">Yazar Şifre</label>
                <input type="password" id="yazarPass" class="form-control">
            </div>
            <button id="yazarGirisBtn" class="btn btn-outline-warning btn-block"><i class="fas fa-pencil-alt"></i>&nbsp;Yazar Girişi Yap</button>
        </div>
        <div class="row mt-2">
            <div class="col-md-12">
                <a href="anasayfa.php" class="btn btn-outline-success btn-block"><i class="fas fa-home"></i>&nbsp;Anasayfaya Dön</a>
                   </div>
            </div>
    </div>
</div>


<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="../js/jquery.js"></script>
<script src="../js/alertify.js"></script>
<script src="../js/giris.js"></script>
<script src="../vendor/twbs/bootstrap/dist/js/bootstrap.js"></script>

</body>
</html>