<?php
session_start();
require_once "db.php";

$cikti = array();
$cikti["hata"]  =FALSE;
$_code = 200;
$_method = $_SERVER["REQUEST_METHOD"];

if($_method == "POST"){
    $gelenMail = addslashes(trim($_POST["gelenMail"]));
    $gelenPass = addslashes(trim($_POST["gelenPass"]));
    if(empty($gelenMail) || empty($gelenPass)){
        $cikti["hata"] = TRUE;
        $cikti["hataMesaji"] = "Parametreler boş gönderilemez.";
        $_code = 400;
    }else{
        $varMi = $db->query("SELECT * FROM admin WHERE adminMail='$gelenMail'")->rowCount();
        if($varMi != 0){
            $cikti["hata"] = TRUE;
            $cikti["hataMesaji"] = "Sistemde bu maile kayıtlı bir admin olduğu için işleminiz gerçekleştirilemiyor.";
            $_code = 400;
        }else{
            $ex = $db->prepare("INSERT INTO admin SET adminMail=:mail, adminPass=:pass");
            $ekle = $ex->execute(array(
                "mail"=>$gelenMail,
                "pass"=>$gelenPass
            ));
            if($ekle){
                $cikti["hata"] = FALSE;
                $cikti["adminler"] = "Admin sisteme başarıyla eklendi";
                $_code = 200;
            }else{
                $cikti["hata"] = TRUE;
                $cikti["hataMesaji"] = "Sistemsel bir hata meydana geldi.Lütfen daha sonra tekrar deneyiniz.";
                $_code = 400;
            }
        }
    }

}else if($_method == "DELETE"){
    if(isset($_GET["gelenId"])){
        $gelenId = $_GET["gelenId"];
        $sil = $db->query("DELETE FROM admin WHERE adminId='$gelenId' ");
        if($sil){
            $cikti["hata"] = FALSE;
            $cikti["adminler"] = "Seçili admin sistemden silindi";
            $_code = 200;
        }else{
            $cikti["hata"] = TRUE;
            $cikti["hataMesaji"] = "Sistemsel bir sorun oluştu.Lütfen daha sonra tekrar deneyiniz.";
            $_code = 400;
        }
    }else{
        $cikti["hata"] = TRUE;
        $cikti["hataMesaji"] = "Delete isteği parametre gönderilmediği için çalışmadı";
        $_code = 400;
    }

}else if($_method == "GET"){
    if(isset($_GET["tumadminler"])){
        $adminSayisi = $db->query("SELECT * FROM admin")->rowCount();
        if($adminSayisi != 0){
            $adminler = $db->query("SELECT * FROM admin")->fetchAll(PDO::FETCH_ASSOC);
            $cikti["hata"] = FALSE;
            $cikti["adminler"] = $adminler;
            $_code = 200;
        }else{
            $cikti["hata"] = TRUE;
            $cikti["hataMesaji"] = "Hiç admin bulunmuyor.";
            $_code = 400;
        }
    }else if(isset($_GET["adminMail"]) && isset($_GET["adminPass"])){
        $gelenMail = addslashes(trim($_GET["adminMail"]));
        $gelenPass = addslashes(trim($_GET["adminPass"]));
        $varMi = $db->query("SELECT * FROM admin WHERE adminMail='$gelenMail' AND adminPass='$gelenPass'")->rowCount();

        if($varMi != 0){
            $_SESSION["adminmail"] = $gelenMail;
            $cikti["hata"] = FALSE;
            $cikti["adminler"] = "Giriş Başarılı Hoşgeldin ".$gelenMail;
            $_code = 200;
        }else{
            $cikti["hata"] = TRUE;
            $cikti["hataMesaji"] = "Sistemde böyle bir admin bulunmuyor";
            $_code = 400;
        }

    }else{
        $cikti["hata"] = TRUE;
        $cikti["hataMesaji"] = "Get isteği boş gönderildiği için işlem yapılamadı.";
        $_code = 400;
    }

}else{
    $cikti["hata"] = TRUE;
    $cikti["hataMesaji"] = "POST || DELETE || GET işlemlerinden birisi yapılabilir.";
    $_code = 400;
}


SetHeader($_code);
$cikti[$_code] = HttpStatus($_code);
echo json_encode($cikti);




?>