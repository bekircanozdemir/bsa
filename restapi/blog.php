<?php
require_once "db.php";
$cikti = array();
$cikti["hata"] = FALSE;
$_code = 200;
$_method = $_SERVER["REQUEST_METHOD"];

if($_method == "POST"){
    $gelenBaslik = addslashes(trim($_POST["baslik"]));
    $gelenMetin  = $_POST["metin"];

    $ex = $db->prepare("INSERT INTO blog SET blogBaslik=:baslik, blogMetin=:metin");
    $ekle = $ex->execute(array(
        "baslik"=>$gelenBaslik,
        "metin"=>$gelenMetin
    ));
    if($ekle){
        $cikti["hata"] = FALSE;
        $cikti["bloglar"] = "Blog Yazısı Eklendi";
        $_code = 200;
    }else{
        $cikti["hata"] = TRUE;
        $cikti["hataMesaji"] = "Sistemsel bir hata meydana geldi.Lütfen daha sonra tekrar deneyiniz.";
        $_code = 400;
    }

}else if($_method == "PUT"){
    $gelen_veriler = json_decode(file_get_contents("php://input"));
    $gelenId = $gelen_veriler->id;
    $gelenBaslik = $gelen_veriler->baslik;
    $gelenMetin = $gelen_veriler->metin;

    $ex = $db->prepare("UPDATE blog  SET blogBaslik=:baslik, blogMetin=:metin WHERE blogId=:id");
    $guncelle = $ex->execute(array(
        "baslik"=>$gelenBaslik,
        "metin"=>$gelenMetin,
        "id"=>$gelenId
    ));
    if($guncelle){
        $cikti["hata"] = FALSE;
        $cikti["bloglar"] = "Blog Yazısı Güncellendi";
        $_code = 200;
    }else{
        $cikti["hata"] = TRUE;
        $cikti["hataMesaji"] = "Sistemsel bir sorun meydana geldi .Lütfen daha sonra tekrar deneyiniz.";
        $_code = 400;
    }

}else if($_method == "GET"){
    if(isset($_GET["tumBloglar"])){
        $varMi = $db->query("SELECT * FROM blog")->rowCount();
        if($varMi != 0){
            $bloglar = $db->query("SELECT * FROM blog")->fetchAll(PDO::FETCH_ASSOC);
            $cikti["hata"] = FALSE;
            $cikti["bloglar"] = $bloglar;
            $_code = 200;
        }else{
            $cikti["hata"] = TRUE;
            $cikti["hataMesaji"] ="Blog Yazısı Yok.";
            $_code = 400;
        }
    }else if(isset($_GET["gelenBlogId"])){
        $gelenId = $_GET["gelenBlogId"];
        $blog = $db->query("SELECT * FROM blog WHERE blogId='$gelenId'")->fetch(PDO::FETCH_ASSOC);
        $cikti["hata"] = FALSE;
        $cikti["bloglar"] = $blog;
        $_code = 200;
    }else if(isset($_GET["arananBlog"])){
        $gelenArama = $_GET["arananBlog"];
        $varMi = $db->query("SELECT * FROM blog WHERE blogBaslik LIKE '%$gelenArama%' OR blogMetin LIKE '%$gelenArama%'")->rowCount();
        if($varMi != 0){
            $bloglar = $db->query("SELECT * FROM blog WHERE blogBaslik LIKE '%$gelenArama%' OR blogMetin LIKE '%$gelenArama%'")->fetchAll(PDO::FETCH_ASSOC);
            $cikti["bloglar"] = $bloglar;
            $cikti["hata"] = FALSE;
            $_code = 200;
        }else{
            $cikti["hata"] = TRUE;
            $cikti["hataMesaji"] = "Aramanızla eşleşen blog bulunamadı";
            $_code = 400;
        }
    }
    else{
        $cikti["hata"] = TRUE;
        $cikti["hataMesaji"] ="Parametre anlaşılamadı.";
        $_code = 400;
    }

}else{
    $cikti["hata"] = TRUE;
    $cikti["hataMesaji"] ="POST || PUT || GET istekleri çalışıyor.Başka istekler devre dışı";
    $_code = 400;
}

SetHeader($_code);
$cikti[$_code] = HttpStatus($_code);
echo json_encode($cikti);

?>