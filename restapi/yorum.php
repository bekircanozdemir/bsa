<?php
require_once "db.php";
$cikti = array();
$cikti["hata"] = FALSE;
$_code =200;
$_method = $_SERVER["REQUEST_METHOD"];

if($_method == "POST"){
    $gelenAd = $_POST["ad"];
    $gelenYorum =$_POST["yorum"];
    $gelenBlogId =$_POST["blogId"];
    $durum = $_POST["durum"];
    if(empty($gelenAd) || empty($gelenYorum) || empty($gelenBlogId)){
        $cikti["hata"] = TRUE;
        $cikti["hataMesaji"] = "Lütfen gerekli kısımları boş bırakmayınız.";
        $_code = 400;
    }else{
        $ex = $db->prepare("INSERT INTO yorum SET yorumYazan=:ad, metin=:metin, kabul=:kabul, blogId=:blogid");
        $ekle = $ex->execute(array(
            "ad"=>$gelenAd,
            "metin"=>$gelenYorum,
            "kabul"=>$durum,
            "blogid"=>$gelenBlogId
        ));
        if($ekle){
            $cikti["hata"] = FALSE;
            $cikti["yorumlar"] = "Yorumunuz sisteme eklendi.Admin kontrolünden sonra onaylanırsa görünecektir.";
            $_code = 200;
        }else{
            $cikti["hata"] = TRUE;
            $cikti["hataMesaji"] = "Sistemsel bir hata meydana geldi.Lütfen daha sonra tekrar deneyiniz.";
            $_code = 400;
        }
    }

}else if($_method == "PUT"){
    $gelen_veri = json_decode(file_get_contents("php://input"));
    $kabul = $gelen_veri->kabul;
    $id = $gelen_veri->id;
    $ex = $db->prepare("UPDATE yorum SET kabul=:kabul WHERE yorumId=:id");
    $guncelle = $ex->execute(array(
        "kabul"=>$kabul,
        "id"=>$id
    ));
    if($guncelle){
        $cikti["hata"] = FALSE;
        $cikti["yorumlar"] = "Yorum onaylandı.";
        $_code = 200;
    }else{
        $cikti["hata"]  = TRUE;
        $cikti["hataMesaji"] = "Sistemsel bir hata meydana geldi.Lütfen daha sonra tekrar deneyiniz.";
        $_code = 400;
    }

}else if($_method == "GET"){
    if(isset($_GET["kullanici"])){
        $gelenId = $_GET["id"];
        $varMi = $db->query("SELECT * FROM yorum WHERE blogId='$gelenId'  AND kabul=1")->rowCount();
        if($varMi != 0){
            $yorumlar = $db->query("SELECT * FROM yorum WHERE blogId='$gelenId' AND kabul=1")->fetchAll(PDO::FETCH_ASSOC);
            $cikti["hata"] = FALSE;
            $cikti["yorumlar"] = $yorumlar;
            $_code = 200;

        }else{
            $cikti["hata"]  = TRUE;
            $cikti["hataMesaji"] = "Yorum yok";
            $_code = 400;
        }
    }else if($_GET["admin"]){
            $varMi = $db->query("SELECT * FROM yorum WHERE kabul=0")->rowCount();
            if($varMi != 0){
                $yorumlar = $db->query("SELECT * FROM yorum WHERE kabul=0")->fetchAll(PDO::FETCH_ASSOC);
                $cikti["hata"] = FALSE;
                $cikti["yorumlar"] = $yorumlar;
                $_code =200;
            }else{
                $cikti["hata"] = TRUE;
                $cikti["hataMesaji"] = "Yorum Yok";
                $_code = 400;
            }
    }

}else{
    $cikti["hata"] = TRUE;
    $cikti["hataMesaji"] = "POST || PUT || GET işlemleri çalışıyor sadece";
    $_code = 400;
}











SetHeader($_code);
$cikti[$_code] = HttpStatus($_code);
echo json_encode($cikti);
?>