<?php
session_start();
require_once "db.php";

$cikti = array();
$cikti["hata"]  =FALSE;
$_code = 200;
$_method = $_SERVER["REQUEST_METHOD"];



if($_method == "POST"){
    if(empty($_POST["gelenMail"]) || empty($_POST["gelenPass"])){
        $cikti["hata"] = TRUE;
        $cikti["hataMesaji"] = "Parametreler eksik gönderilemez.";
        $_code = 400;
    }else{
        $gelenMail = addslashes(trim($_POST["gelenMail"]));
        $gelenPass = addslashes(trim($_POST["gelenPass"]));
        $varMi = $db->query("SELECT * FROM yazarlar WHERE yazarMail='$gelenMail'")->rowCount();
        if($varMi != 0){
            $cikti["hata"] = TRUE;
            $cikti["hataMesaji"] = "Sistemde bu mail adresine kayıtlı bir yazar bulunduğu için ekleme işlemi yapılamadı.";
            $_code = 400;
        }else{
            $ex = $db->prepare("INSERT INTO yazarlar SET yazarMail=:mail, yazarPass=:pass");
            $ekle = $ex->execute(array(
                "mail"=>$gelenMail,
                "pass"=>$gelenPass
            ));
            if($ekle){
                $cikti["hata"] = FALSE;
                $cikti["yazarlar"] = "Yazar sisteme eklendi";
                $_code = 200;
            }else{
                $cikti["hata"] = TRUE;
                $cikti["hataMesaji"] = "Sistemsel bir sorun oluştu.Lütfen daha sonra tekrar deneyiniz.";
                $_code = 400;
            }
        }
    }

}else if($_method == "PUT"){
    $gelen_veriler = json_decode(file_get_contents("php://input"));
    if(isset($gelen_veriler->yazarId) && isset($gelen_veriler->yazarMail) && isset($gelen_veriler->yazarPass)){
        $yazarId =  $gelen_veriler->yazarId;
        $yazarMail= $gelen_veriler->yazarMail;
        $yazarPass = $gelen_veriler->yazarPass;
        $varMi = $db->query("SELECT * FROM yazarlar WHERE yazarId='$yazarId'")->rowCount();

            if($varMi != 0){
                $ex = $db->prepare("UPDATE yazarlar SET yazarMail=:mail, yazarPass=:pass WHERE yazarId=:id");
                $guncelle = $ex->execute(array(
                    "mail"=>$yazarMail,
                    "pass"=>$yazarPass,
                    "id"=>$yazarId
                ));
                if($guncelle){
                    $cikti["hata"] = FALSE;
                    $cikti["yazarlar"] = "Yazarlar Güncellendi. ";
                    $_code = 200;
                }else{
                    $cikti["hata"] = TRUE;
                    $cikti["hataMesaji"] = "Parametreler boş gönderildiği için güncelleme yapılmadı. ";
                    $_code = 400;
                }
                }else{
                $cikti["hata"] = TRUE;
                $cikti["hataMesaji"] = "Parametreler boş gönderildiği için güncelleme yapılmadı. ";
                $_code = 400;
                }
    }else{
        $cikti["hata"] = TRUE;
        $cikti["hataMesaji"] = "Parametreler boş gönderildiği için güncelleme yapılmadı. ";
        $_code = 400;
    }

}else if($_method == "DELETE"){
    if(isset($_GET["gelenId"])){
        $gelenId = $_GET["gelenId"];
        $sil = $db->query("DELETE FROM yazarlar WHERE yazarId='$gelenId'");
        if($sil){
            $cikti["hata"] = FALSE;
            $cikti["yazarlar"] = "Yazar silindi";
            $_code = 200;

        }else{
            $cikti["hata"] = TRUE;
            $cikti["hataMesaji"] = "Bu id ye sahip bir yazar olmadığı için kimse silinmedi. ";
            $_code = 400;
        }
    }else{
        $cikti["hata"] = TRUE;
        $cikti["hataMesaji"] = "Delete isteği parametre gönderilmediği için çalışmadı.";
        $_code = 400;
    }

}else if($_method == "GET"){
    if(isset($_GET["tumYazarlar"])){
        $varMi = $db->query("SELECT * FROM yazarlar")->rowCount();
        if($varMi != 0){
            $yazarlar = $db->query("SELECT * FROM yazarlar")->fetchAll(PDO::FETCH_ASSOC);
            $cikti["hata"] = FALSE;
            $cikti["yazarlar"] = $yazarlar;
            $_code = 200;
        }else{
            $cikti["hata"] = TRUE;
            $cikti["hataMesaji"] = "Sistemde yazar bulunmuyor.";
            $_code = 400;
        }

    }else if(!empty($_GET["tekliYazar"])){
            $gelenId = $_GET["tekliYazar"];
            $yazar = $db->query("SELECT * FROM yazarlar WHERE yazarId='$gelenId'")->fetch(PDO::FETCH_ASSOC);
            $cikti["hata"] = FALSE;
            $cikti["yazarlar"] = $yazar;
            $_code = 200;

    }else if(isset($_GET["yazarMail"]) && isset($_GET["yazarPass"])){
        $gelenMail = addslashes(trim($_GET["yazarMail"]));
        $gelenPass = addslashes(trim($_GET["yazarPass"]));
            $varMi = $db->query("SELECT * FROM yazarlar WHERE yazarMail='$gelenMail' AND yazarPass='$gelenPass'")->rowCount();
            if($varMi != 0){
                $_SESSION["yazarmail"] = $gelenMail;
                $cikti["hata"] = FALSE;
                $cikti["yazarlar"]  = "Sisteme hoş geldiniz.";
                $_code = 200;

            }else{
                $cikti["hata"] = TRUE;
                $cikti["hataMesaji"] = "Sistemde böyle bir yazar bulunamadı";
                $_code = 400;
            }
    }else{
        $cikti["hata"] = TRUE;
        $cikti["hataMesaji"] = "Get isteğine parametre göndermediğiniz için işlem yapılamadı.";
        $_code = 400;
    }

}else{
    $cikti["hata"] = TRUE;
    $cikti["hataMesaji"] = "POST || PUT || DELETE || GET isteklerini atabilirsiniz sadece.";
    $_code = 400;
}















SetHeader($_code);
$cikti[$_code] = HttpStatus($_code);
echo json_encode($cikti);


?>